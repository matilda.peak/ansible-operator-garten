# Ansible Operator - Garten
An Ansible-based Operator, for the corresponding [Garten Role].

Minimal files to form a functional operator. In this early version
we just introduce the conventional `build` and `deploy` directories.
There should be much more for testing but, for now, this is all we need.

Our operator logic is encoded in a separate Ansible Role that's
installed from [Ansible Galaxy] during the build. This way we keep our
application logic and operator implementation separate.
 
The documentation for the [Ansible SDK] has plenty of information on
building and deploying operators, what follows is a summary of what you can
find there.  

>   For deployment instructions refer to our [chronicler] peer project,
    whose structure is identical to this project.

>   You will need to provide suitable TfL (Transport for London) application
    credentials and provide the service name of the Chronicler instance that
    you've deployed. Set appropriate values in the Custom Resource (cr)
    files in `deploy/crds`.

---

[ansible galaxy]: https://galaxy.ansible.com/matildapeak/garten
[ansible sdk]: https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md
[chronicler]: https://gitlab.com/matilda.peak/ansible-operator-chronicler
[garten role]: https://github.com/MatildaPeak/ansible-role-garten
